#include "UnoJoy.h"
int clickEvent=0;
int down=0;
int high=0;
int right=0;
int left=0;
int posX = 0;
int posY = 0;
void setup(){
  pinMode(2,INPUT_PULLUP);
  pinMode(6,INPUT_PULLUP);
  pinMode(7,INPUT_PULLUP);
  pinMode(8,INPUT_PULLUP);
  pinMode(9,INPUT_PULLUP);
  setupUnoJoy();
  //Serial.begin(9600);
}

void loop(){
  
/* clickEvent= digitalRead(2);
 down= digitalRead(8);
 high= digitalRead(7); 
 right= digitalRead(6);
 left= digitalRead(9);
 Serial.println(clickEvent);
 Serial.println(down);
 Serial.println(high);
 Serial.println(right);
 Serial.println(left);*/
/* posX = analogRead(A0);
 Serial.println(posX);
 posY = analogRead(A1);
 Serial.println(posY);
 */
  // Leggiamo i comandi
  dataForController_t controllerData = getControllerData();
 
  //Inviamo i comandi al controller
  setControllerData(controllerData);

  //delay(100);
}

dataForController_t getControllerData(void){
  // Inizializziamo i dati del controller con input nulli
  dataForController_t controllerData = getBlankDataForController();
   
  // Mappiamo i dati dal potenziometro sull'asse X dell'analogico sinistro.
  // Dal momomento che analogRead(pin) ritorna un valore a 10 bit occorre
  // effettuare uno shift per tralasciare i 2 bit meno significativi. 
  controllerData.leftStickX = analogRead(A0) >> 2;
  controllerData.leftStickY = analogRead(A1) >> 2;
 
  // Mappiamo i dati del pulsante sul pulsante L3
  // Il valore deve essere negato perché il pulsante è attivo quando il segnale è basso
  controllerData.l3On = !digitalRead(2);
  
  controllerData.dpadUpOn = digitalRead(7);
  controllerData.dpadDownOn = digitalRead(8);
  controllerData.dpadLeftOn = digitalRead(9);
  controllerData.dpadRightOn = digitalRead(6);
 
  return controllerData;
}

/*
 * 
 
void setup(){
  pinMode(2, INPUT);
  pinMode(3, INPUT);
  setupUnoJoy();
}
 
void loop(){
  // Leggiamo i comandi
  dataForController_t controllerData = getControllerData();
 
  //Inviamo i comandi al controller
  setControllerData(controllerData);
}
 
dataForController_t getControllerData(void){
  // Inizializziamo i dati del controller con input nulli
  dataForController_t controllerData = getBlankDataForController();
   
  // Mappiamo i dati dal potenziometro sull'asse X dell'analogico sinistro.
  // Dal momomento che analogRead(pin) ritorna un valore a 10 bit occorre
  // effettuare uno shift per tralasciare i 2 bit meno significativi. 
  controllerData.leftStickX = analogRead(A2) >> 2;
 
  // Mappiamo i dati del pulsante sul pulsante X
  // Il valore deve essere negato perché il pulsante è attivo quando il segnale è basso
  controllerData.crossOn = !digitalRead(2);
 
  return controllerData;
}
 */
