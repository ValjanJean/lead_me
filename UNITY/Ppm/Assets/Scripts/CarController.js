﻿#pragma strict

var wheelColliders : WheelCollider[] = new WheelCollider[4];
var tires : Transform[] = new Transform[4];

var maxTorque : float = 150;
var maxSteer : float = 35;

var centerOfMass : Transform;

private var rigidBody : Rigidbody;

function Start(){
	rigidBody = GetComponent(Rigidbody);
	rigidBody.centerOfMass = centerOfMass.localPosition;
}


function Update(){
	updateTires();
	for (var i : int=0; i<4; i++){
		if (Input.GetKeyDown("joystick button 10")){
			wheelColliders[i].brakeTorque=1000;
	}
		if (Input.GetKeyUp("joystick button 10")){
			wheelColliders[i].brakeTorque=0;
	}
	}
	
}

function FixedUpdate() { 
	var accellerate : float = Input.GetAxis("Vertical");
	var steer : float = Input.GetAxis("Horizontal");	

	var finalAngle : float = steer*maxSteer;

	wheelColliders[0].steerAngle = finalAngle;
	wheelColliders[1].steerAngle = finalAngle;

	for (var i : int=0; i<4; i++){
		wheelColliders[i].motorTorque=accellerate*maxTorque;
	}
/*	if (Input.GetKeyUp("joystick button 10")){
			wheelColliders[i].brakeTorque=0;
	}*/

	}
	
	


function updateTires (){
	for (var i : int=0; i<4; i++){
		var rot : Quaternion;
		var pos : Vector3;
	
		wheelColliders[i].GetWorldPose(pos, rot);

		tires[i].position=pos;
		tires[i].rotation=rot;
		
	}
}