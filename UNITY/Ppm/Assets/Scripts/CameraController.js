﻿#pragma strict

var lookAt : Transform;
var camTransform : Transform;

private var distance : float = 10.0;
private var currentX : float = 0.0;
private var currentY : float = 0.0;
private var sensitivityX : float = 4.0;
private var sensitivityY : float = 1.0;
private var maxY : float = 50.0;
private var minY : float = 0.0;

var dir : Vector3 = new Vector3(0, 0, -distance);

function Start(){
	camTransform = transform;	
}

function Update(){
	currentX += Input.GetAxis("Horizontal1")*sensitivityX;
	currentY += Input.GetAxis("Vertical1")*sensitivityY;
	currentY = Mathf.Clamp(currentY, minY, maxY);
	if (Input.GetKey ("escape")) {
        	Application.Quit();
    }
	if (Input.GetKey(KeyCode.R)) {
		SceneManagement.SceneManager.LoadScene("Main");
		}
}

function LateUpdate(){	
	var rot : Quaternion = Quaternion.Euler(currentY, currentX, 0);
	camTransform.position = lookAt.position + rot*dir;
	camTransform.LookAt(lookAt.position);
}